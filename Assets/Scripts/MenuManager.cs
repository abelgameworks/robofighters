using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public GameObject MainMenu;
    public GameObject ControlsMenu;

    public void ShowControls()
    {
        MainMenu.SetActive(false);
        ControlsMenu.SetActive(true);
    }

    public void HideControls()
    {
        MainMenu.SetActive(true);
        ControlsMenu.SetActive(false);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerScheme : MonoBehaviour
{
    public KeyCode forward;
    public KeyCode back;
    public KeyCode attack;
    public KeyCode defend;
}

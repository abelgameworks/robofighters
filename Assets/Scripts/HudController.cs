using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudController : MonoBehaviour
{
    public PlayerHud Player1;
    public PlayerHud Player2;
    public Text TimeLeft;
    public GameObject Countdown;
    public GameObject Fight;
    public GameObject Draw;

    public void SetTimeLeft(int value)
    {
        TimeLeft.text = value.ToString();
    }

    public void SetCountDown(int value)
    {
        Countdown.GetComponent<Text>().text = value.ToString();
        Countdown.SetActive(true);
    }

    public void EnableCountDown(bool value)
    {
        Countdown.SetActive(value);
    }

    public void EnableFight(bool value)
    {
        Fight.SetActive(value);
    }

    public void EnableDraw(bool value)
    {
        Draw.SetActive(value);
    }
}

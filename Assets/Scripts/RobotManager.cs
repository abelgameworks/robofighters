using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RobotActions), typeof(ControllerScheme))]
public class RobotManager : MonoBehaviour
{
    public GameObject Model;
    public Animator Anim;
    public RobotActions actions;
    public ControllerScheme controls;
    public int currentRoundsWon;
    public float RemainingDefense;
    public float TotalDefense;

    public bool facingRight = false;
    public bool attacking = false;
    public bool defending = false;

    public GameObject Shield;

    public bool overload = false;
    public bool attackingOverload = false;
    public bool defendingOverload = false;

    private float StopTimer;
    private float OverloadTimer;
    public int AttackLimit = 3;
    public int DefenseLimit = 100;
    private int CurrentDefense = 0;
    public int AttackPower = 5;
    public int OverloadLimit = 1;
    public int MaxHealth = 50;
    private int currentHealth = 50;

    public bool canMove = false;
    public bool roundLost = false;

    public int MaxOverload = 4;
    public int CurrentOverload = 0;

    public bool walking = false;
    public float Speed = 0;
    public GameObject WalkingReference; 
    public GameObject IdleReference;
    public GameObject BackSmoke;
    public GameObject FrontSmoke;

    public PlayerHud hud;

    public AudioPlayer audioPlayer;
    public AudioSource steps;
    private bool stepSound = false;

    private void Start()
    {
        audioPlayer = GameObject.Find("Sounds").GetComponent<AudioPlayer>();
    }

    public void StartRobot()
    {
        CurrentDefense = 0;
        currentHealth = MaxHealth;
        actions.ResetRobotRotation();
        Model.transform.position = actions.StartingPosition.transform.position;
        Model.transform.rotation = actions.StartingPosition.transform.rotation;
        attacking = false;
        defending = false;
        roundLost = false;
        canMove = true;

        hud.SetShield(CurrentDefense);
        hud.SetHealth(MaxHealth);
    }

    public void EnableInputs(bool value)
    {
        canMove = value;
    }

    public void WinRound()
    {
        currentRoundsWon++;
        hud.UpdateRoundsWon(currentRoundsWon);
    }

    public void PlayStep()
    {
        steps.Play();
    }

    public void moveRobot()
    {
        if (Input.GetKey(controls.forward))
        {
            Model.transform.position = new Vector3(Model.transform.position.x, WalkingReference.transform.position.y, Model.transform.position.z);
            Anim.SetBool("Walking", true);
            Model.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
            if (actions.RobotType == RobotActions.Type.Robotank && !stepSound)
            {
                steps.Play();
                stepSound = true;
                BackSmoke.SetActive(true);
            }
        }
        else if (Input.GetKey(controls.back))
        {
            Model.transform.position = new Vector3(Model.transform.position.x, WalkingReference.transform.position.y, Model.transform.position.z);
            Anim.SetBool("Walking", true);
            Model.transform.Translate(Vector3.forward * Speed * Time.deltaTime * -1);
            if (actions.RobotType == RobotActions.Type.Robotank && !stepSound)
            {
                steps.Play();
                stepSound = true;
                FrontSmoke.SetActive(true);
            }
        }
        else
        {
            Model.transform.position = new Vector3(Model.transform.position.x, IdleReference.transform.position.y, Model.transform.position.z);
            Anim.SetBool("Walking", false);
            if (actions.RobotType == RobotActions.Type.Robotank && stepSound)
            {
                steps.Pause();
                stepSound = false;
                FrontSmoke.SetActive(false);
                BackSmoke.SetActive(false);
            }
        }
    }

    public void Attack()
    {
        if (!actions.SpeedLimit)
        {
            if (attacking)
            {
                actions.IncreaseRotationSpeed();
            }
            else
            {
                actions.StartRotation();
                attacking = true;
            }
        }
        else {
            if (CurrentOverload < MaxOverload)
            {
                CurrentOverload++;
                actions.ForceIncreaseRotationSpeed();
                hud.SetLettersRed(CurrentOverload);
            }
            else {
                actions.StopRotation();
                attackingOverload = true;
                attacking = false;
                CurrentOverload = 0;
            }
        }
    }

    public void DefenseUp()
    {
        if (CurrentDefense < DefenseLimit)
        {
            if (!defending)
            {
                actions.StartDefense();
                Shield.SetActive(true);
                defending = true;
                audioPlayer.PlayShield();
            }
        }
        else
        {
            defendingOverload = true;
            audioPlayer.StopAudio();
        }
    }

    public void DefenseDown()
    {
        actions.StopDefense();
        Shield.SetActive(false);
        audioPlayer.StopAudio();
    }

    public void RecoverDefense()
    {
        CurrentDefense -= 1;
        hud.SetShield(CurrentDefense);
    }

    public void RecieveHit(int damageRecieved)
    {
        if (!defending)
        {
            currentHealth -= damageRecieved;
            hud.SetHealth(currentHealth);
            if (currentHealth <= 0)
            {
                roundLost = true;
            }
        } else
        {
            CurrentDefense += damageRecieved;
            hud.SetShield(CurrentDefense);
            if (CurrentDefense >= DefenseLimit)
            {
                defending = false;
                defendingOverload = true;
                DefenseDown();
            }
        }
    }

    public void CheckInputs()
    {
        if (canMove)
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;

            moveRobot();

            if (attackingOverload && defendingOverload)
            {
                overload = true;
                attackingOverload = false;
                defendingOverload = false;
                hud.SetShieldRed();
                hud.SetLettersRed(4);
                OverloadTimer = 0f;
            }

            if (!overload)
            {

                if (Input.GetKeyDown(controls.attack) && !defending && !attackingOverload)
                {
                    Attack();
                    StopTimer = 0f;
                }

                if (Input.GetKey(controls.defend) && !attacking && !defendingOverload)
                {
                    DefenseUp();
                    StopTimer = 0f;
                }

                if (Input.GetKeyUp(controls.defend))
                {
                    DefenseDown();
                    defending = false;
                }

                if (attacking)
                {
                    actions.Rotate();
                }

                if (defending)
                {
                    DefenseUp();
                }

                if (!defending && CurrentDefense != 0)
                {
                    RecoverDefense();
                }

                if (attacking || defending)
                {
                    StopTimer += Time.deltaTime;

                    if (StopTimer >= AttackLimit)
                    {
                        actions.StopRotation();
                        attacking = false;
                    }

                    if (StopTimer >= DefenseLimit)
                    {
                        defending = false;
                        defendingOverload = true;
                    }
                }


                if (attackingOverload || defendingOverload)
                {
                    OverloadTimer += Time.deltaTime;
                    if (OverloadTimer >= OverloadLimit)
                    {
                        if (attackingOverload)
                        {
                            attackingOverload = false;
                            attacking = false;
                            actions.SpeedLimit = false;
                        }
                        else
                        {
                            defendingOverload = false;
                            defending = false;
                        }
                        OverloadTimer = 0;
                    }
                }
            }
            else
            {
                OverloadTimer += Time.deltaTime;

                if (OverloadTimer > OverloadLimit)
                {
                    overload = false;
                    hud.SetLettersBlack();
                    hud.SetShieldBlack();
                    CurrentDefense = 0;
                    hud.SetShield(CurrentDefense);
                }
            }
        }
        else {
            steps.Pause();
            FrontSmoke.SetActive(false);
            BackSmoke.SetActive(false);
        }
    }
}

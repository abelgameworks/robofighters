using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHud : MonoBehaviour
{
    public Text Ov;
    public Text Er;
    public Text Lo;
    public Text Ad;
    public Text Shield;
    public GameObject Round1;
    public GameObject Round2;
    public GameObject Round3;
    public Scrollbar HealthBar;
    public GameObject WinHolder;

    public void SetShield(int value)
    {
        Shield.text = value.ToString();
    }

    public void SetShieldRed()
    {
        Shield.color = Color.red;
    }

    public void SetShieldBlack()
    {
        Shield.color = Color.black;
    }

    public void SetLettersRed(int value)
    {
        if (value == 1)
        {
            Ov.color = Color.red;
        }
        else if (value == 2)
        {
            Er.color = Color.red;
        }
        else if (value == 3)
        {
            Lo.color = Color.red;
        }
        else if (value == 4)
        {
            Ad.color = Color.red;
        }
        else if (value == 5)
        {
            Ov.color = Color.red;
            Er.color = Color.red;
            Lo.color = Color.red;
            Ad.color = Color.red;
        }
    }

    public void SetLettersBlack()
    {
        Ov.color = Color.black;
        Er.color = Color.black;
        Lo.color = Color.black;
        Ad.color = Color.black;
    }

    public void SetHealth(int health)
    {
        float Value = health*0.02f;
        HealthBar.size = Value;
    }

    public void UpdateRoundsWon(int won)
    {
        if (won == 1)
        {
            Round1.SetActive(true);
        }
        else if (won == 2)
        {
            Round2.SetActive(true);
        }
        else if (won == 3)
        {
            Round3.SetActive(true);
        }
    }

    public void ShowWinHolder(bool value)
    {
        WinHolder.SetActive(value);
    }
}

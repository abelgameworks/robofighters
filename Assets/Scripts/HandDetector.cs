using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandDetector : MonoBehaviour
{
    public RobotActions.Type RobotType;
    public RobotManager RobotManager;
    public RobotManager EnemyRobotManager;
    public AudioPlayer audioPlayer;
    public GameObject prefab;

    private void Start()
    {
        audioPlayer = GameObject.Find("Sounds").GetComponent<AudioPlayer>();
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (RobotManager.attacking)
        {
            if (EnemyRobotManager.actions.RobotType.ToString().Equals(collider.gameObject.tag))
            {
                EnemyRobotManager.RecieveHit(RobotManager.AttackPower);
                audioPlayer.PlayHit();
                Vector3 pos = collider.ClosestPoint(transform.position);
                GameObject effect = Instantiate(prefab);
                effect.transform.position = pos;
            }

            if (collider.gameObject.tag.Equals("Shield"))
            {
                EnemyRobotManager.RecieveHit(RobotManager.AttackPower);
            }
        }

        RobotManager.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    private void OnTriggerExit(Collider other)
    {
        RobotManager.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}

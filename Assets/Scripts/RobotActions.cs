using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotActions : MonoBehaviour
{
    public enum Type { Robohead, Robotank }
    public Type RobotType;

    public float RotationSpeed = 1.5f;
    public float IncreaseSpeed = 1f;
    public float MaxSpeed = 8f;
    public float CurrentSpeed = 0f;
    public bool SpeedLimit = false;

    float ValueX = 0;
    float ValueY = 0;
    float ValueZ = 0;

    public GameObject ElementToRotate1;
    public GameObject ElementToRotate2;

    public GameObject UpperArmRight;
    public GameObject UpperArmLeft;
    public GameObject LowerArmRight;
    public GameObject LowerArmLeft;

    public GameObject SpineBone;

    public GameObject UpperLeftArmsDefenseReference;
    public GameObject UpperRightArmsDefenseReference;
    public GameObject LowerLeftArmsDefenseReference;
    public GameObject LowerRightArmsDefenseReference;

    public GameObject UpperLeftArmAttackReference;
    public GameObject UpperRightArmAttackReference;
    public GameObject LowerLeftArmAttackReference;
    public GameObject LowerRightArmAttackReference;

    public GameObject IdleBodyReference;
    public GameObject UpperRightArmIdleReference;
    public GameObject UpperLeftArmIdleReference;
    public GameObject LowerLeftArmIdleReference;
    public GameObject LowerRightArmIdleReference;

    public GameObject StartingPosition;

    public void ResetRobotRotation()
    {
        StopDefense();
    }

    public void Rotate()
    {
        if (RobotType == Type.Robotank)
        {
            ElementToRotate1.transform.Rotate(ValueX, ElementToRotate1.transform.rotation.y + CurrentSpeed, ValueZ);
            ElementToRotate2.transform.Rotate(ValueX, ElementToRotate2.transform.rotation.y + CurrentSpeed, ValueZ);
        }
        else if (RobotType == Type.Robohead)
        {
            ElementToRotate1.transform.Rotate(ElementToRotate1.transform.rotation.x + CurrentSpeed, ValueY, ValueZ);
            UpperArmRight.transform.rotation = UpperRightArmAttackReference.transform.rotation;
            UpperArmLeft.transform.rotation = UpperLeftArmAttackReference.transform.rotation;
            LowerArmRight.transform.rotation = LowerRightArmAttackReference.transform.rotation;
            LowerArmLeft.transform.rotation = LowerLeftArmAttackReference.transform.rotation;
        }
    }

    public void StartRotation()
    {
        CurrentSpeed = RotationSpeed;
    }

    public void StopRotation()
    {
        CurrentSpeed = 0f;
        if (RobotType == Type.Robohead)
        {
            StopDefense();
        }
    }

    public void ForceIncreaseRotationSpeed()
    {
        CurrentSpeed += IncreaseSpeed / 2;
    }

    public void IncreaseRotationSpeed()
    {
        if (RobotType == Type.Robotank)
        {
            if (CurrentSpeed >= MaxSpeed)
            {
                if (CurrentSpeed <= MaxSpeed / 2)
                {
                    CurrentSpeed += IncreaseSpeed / 2;
                }
                else
                {
                    CurrentSpeed += IncreaseSpeed;
                }
            }
            if(CurrentSpeed <= MaxSpeed)
            {
                SpeedLimit = true;
            }
        }
        else if (RobotType == Type.Robohead)
        {
            if (CurrentSpeed <= MaxSpeed)
            {
                if (CurrentSpeed <= MaxSpeed / 2)
                {
                    CurrentSpeed += IncreaseSpeed / 2;
                }
                else
                {
                    CurrentSpeed += IncreaseSpeed;
                }
            }
            if (CurrentSpeed >= MaxSpeed)
            {
                SpeedLimit = true;
            }
        }
    }

    public void StartDefense()
    {
        UpperArmRight.transform.rotation = UpperRightArmsDefenseReference.transform.rotation;
        UpperArmLeft.transform.rotation = UpperLeftArmsDefenseReference.transform.rotation;
        if (RobotType == Type.Robohead)
        {
            SpineBone.transform.rotation = IdleBodyReference.transform.rotation;
            LowerArmRight.transform.rotation = LowerRightArmsDefenseReference.transform.rotation;
            LowerArmLeft.transform.rotation = LowerLeftArmsDefenseReference.transform.rotation;
        }
    }

    public void StopDefense()
    {
        UpperArmRight.transform.rotation = UpperRightArmIdleReference.transform.rotation;
        UpperArmLeft.transform.rotation = UpperLeftArmIdleReference.transform.rotation;
        if (RobotType == Type.Robohead)
        {
            SpineBone.transform.rotation = IdleBodyReference.transform.rotation;
            LowerArmRight.transform.rotation = LowerRightArmIdleReference.transform.rotation;
            LowerArmLeft.transform.rotation = LowerLeftArmIdleReference.transform.rotation;
        }
    }
}

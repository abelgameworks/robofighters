using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    //References to each robot
    public RobotManager Robohead;
    public RobotManager Robotank;

    public GameObject RobotankDeadPosition;
    public GameObject RoboheadDeadPosition;

    public bool RoundFinished = false;
    public int currentRound = 0;
    public int MaxRounds = 5;
    private float RoundTimer = 30f;
    private float EndTimer = 0f;

    private float Speed = 0.1f;
    private float timeCount = 0.0f;

    public HudController hud;

    private bool headRotated = false;
    private bool Restarting = false;

    public AudioPlayer audioPlayer;

    private void Start()
    {
        SetUp();
        ActivateRobots();
        audioPlayer = GameObject.Find("Sounds").GetComponent<AudioPlayer>();
    }

    private void SetUp()
    {
        StartRound();
        headRotated = false;
        RoundTimer = 30f;
        hud.SetTimeLeft((int)RoundTimer);
        Restarting = false;
        RoundFinished = false;
    }

    // Start is called before the first frame update
    void StartRound()
    {
        Robotank.StartRobot();
        Robohead.StartRobot();
    }

    void ActivateRobots()
    {
        Robotank.EnableInputs(true);
        Robohead.EnableInputs(true);
    }

    void PlayFanfare()
    {
        audioPlayer.StopAudio();
        audioPlayer.PlayFanfare();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentRound < MaxRounds || Robotank.currentRoundsWon == 3 || Robotank.currentRoundsWon == 3)
        {
            if (!Restarting)
            {
                if (!RoundFinished && RoundTimer > 0)
                {
                    RoundTimer -= Time.deltaTime;
                    hud.SetTimeLeft((int)RoundTimer);
                    if (!Robotank.roundLost && !Robohead.roundLost)
                    {

                        Robotank.CheckInputs();
                        Robohead.CheckInputs();
                    }
                    else
                    {
                        Robohead.EnableInputs(false);
                        Robotank.EnableInputs(false);
                        if (Robohead.roundLost)
                        {
                            Robotank.WinRound();
                        }
                        else
                        {
                            Robohead.WinRound();
                        }
                        currentRound++;
                        RoundFinished = true;
                        timeCount = 0.0f;
                    }
                }
                else if (RoundTimer <= 0)
                {
                    hud.EnableDraw(true);
                }
                else
                {
                    if (Robotank.roundLost)
                    {
                        if (!Robotank.transform.rotation.Equals(RobotankDeadPosition.transform.rotation) && !headRotated)
                        {
                            Robotank.transform.rotation = Quaternion.Lerp(Robotank.transform.rotation, RobotankDeadPosition.transform.rotation, timeCount * Speed);
                            timeCount = timeCount + Time.deltaTime;
                            EndTimer = 0f;
                        }
                        else
                        {
                            headRotated = true;
                            if (EndTimer < 1f)
                            {
                                Robohead.hud.ShowWinHolder(true);
                                EndTimer += Time.deltaTime;
                            }
                            else
                            {
                                PlayFanfare();
                                Restarting = true;
                                Robohead.hud.ShowWinHolder(false);
                                timeCount = 0f;
                            }
                        }
                    }
                    else if (Robohead.roundLost)
                    {
                        if (!headRotated)
                        {
                            Robohead.transform.Rotate(-90, 0, 0);
                            headRotated = true;
                            EndTimer = 0f;
                        }
                        else
                        {
                            if (EndTimer < 1f)
                            {
                                Robotank.hud.ShowWinHolder(true);
                                EndTimer += Time.deltaTime;
                            }
                            else
                            {
                                PlayFanfare();
                                Restarting = true;
                                Robotank.hud.ShowWinHolder(false);
                                timeCount = 0f;
                            }
                        }
                    }
                }
            }
            else if (Restarting)
            {
                if (timeCount < 3)
                {
                    hud.SetCountDown(3 - (int)timeCount);
                    timeCount += Time.deltaTime;
                }
                else
                {
                    Debug.Log("Restarting");
                    timeCount = 4;
                    hud.EnableCountDown(false);
                    hud.EnableFight(true);
                    Thread.Sleep(500);
                    Debug.Log("Post-sleep");
                    SetUp();
                    hud.EnableFight(false);
                    ActivateRobots();
                    Restarting = false;
                }
            }

        }
        else if (Robotank.currentRoundsWon == 3)
        {
            // Show Robotank wins screen
            SceneManager.LoadScene("RobotankWins");
        }
        else if (Robohead.currentRoundsWon == 3)
        {
            // Show robohead wins screen
            SceneManager.LoadScene("RoboheadWins");
        }
        else if (currentRound > MaxRounds)
        {
            if (Robohead.currentRoundsWon > Robotank.currentRoundsWon)
            {
                SceneManager.LoadScene("RobotankWins");
            }
            else if (Robohead.currentRoundsWon > Robotank.currentRoundsWon)
            {
                SceneManager.LoadScene("RoboheadWins");
            }
            else
            {
                // Show draw screen
                Debug.Log("Draw game");
                SceneManager.LoadScene("Draw");
            }
        }

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

/// <summary>
/// Class that manages the behaciour of the buttons
/// </summary>
public class ButtonBehaviour : MonoBehaviour, IPointerClickHandler
{
    private enum ButtonType { NewGame, Exit, ReturnToMenu, Controls, HideControls } // possible button types

    [SerializeField]
    private ButtonType Type; // type instance from the enum

    public MenuManager menuManager;

    public void OnPointerClick(PointerEventData eventData) // method called upon clicking a button
    {
        if (Type == ButtonType.NewGame)
        {
            SceneManager.LoadScene("Battle");
        }
        if (Type == ButtonType.Exit)
        {
            Application.Quit();
        }
        if (Type == ButtonType.ReturnToMenu)
        {
            SceneManager.LoadScene("MainMenu");
        }
        if (Type == ButtonType.Controls)
        {
            menuManager.ShowControls();
        }
        if (Type == ButtonType.HideControls)
        {
            menuManager.HideControls();
        }
    }
}

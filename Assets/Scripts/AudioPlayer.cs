using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    public List<AudioClip> hitSounds;
    public AudioClip WinFanfare;
    public AudioClip Shield;
    public AudioSource Source;

    public void PlayShield()
    {
        Source.clip = Shield;
        Source.Play();
    }

    public void PlayHit()
    {
        int i = Random.Range(0, 8);
        Source.clip = hitSounds[i];
        Source.Play();
    }

    public void PlayFanfare()
    {
        Source.clip = WinFanfare;
        Source.Play();
    }

    public void StopAudio()
    {
        Source.Pause();
    }
}
